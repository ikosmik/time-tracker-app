package com.zoho.app.frontdesk.android.securitytracker.backgroundProcess;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.zoho.app.frontdesk.android.securitytracker.Constants.MainConstants;
import com.zoho.app.frontdesk.android.securitytracker.Constants.NumberConstants;
import com.zoho.app.frontdesk.android.securitytracker.MainActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SendSMS {

    private Context context;
    private String number;
    private String message;
    //
    public SendSMS(Context context,String number,String message)
    {
        this.context = context;
        this.number = number;
        this.message = message;
        RunInBackground runInBackground = new RunInBackground();
        runInBackground.execute();
    }
    //

    public String restAPICall(String restApiMethod, URL url, String setParameter) throws IOException {
        String responseString = MainConstants.kConstantEmpty;
//        if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
//            Log.d("DSK RestapiMethod", "" + restApiMethod);
//            Log.d("DSK RestapiURL", "" + url + setParameter);
//        }
        if (restApiMethod != null && !restApiMethod.isEmpty() && url != null && !url.toString().isEmpty()) {
            HttpURLConnection con = null;

            con = (HttpURLConnection) url.openConnection();
            if (con != null) {
                con.setDoOutput(true);
                con.setRequestMethod(restApiMethod);
                con.setConnectTimeout(NumberConstants.connectionTimeout);
                con.setReadTimeout(NumberConstants.connectionTimeout);

                if (restApiMethod == MainConstants.kConstantPostMethod && setParameter != null && !setParameter.isEmpty()) {
                    con.setRequestProperty(MainConstants.kConstantUserAgent,
                            MainConstants.user_Agent);
                    con.setRequestProperty(MainConstants.kConstantAcceptLanguage,
                            MainConstants.acceptLanguage);
                    DataOutputStream dataOutputStream = new DataOutputStream(
                            con.getOutputStream());
                    if (dataOutputStream != null) {
                        dataOutputStream.writeBytes(setParameter);
                        dataOutputStream.flush();
                        dataOutputStream.close();
                    }
                } else {
//                    con.connect();
                }
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.d("DSK restApiStatus ", "message " + con.getResponseMessage() + "");
                }
                int status = con.getResponseCode();
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.d("DSK restApiStatusCode", status + "");
                }
                if (status == NumberConstants.kConstantSuccessCode) {
                    // read the response
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));

                    StringBuffer response = new StringBuffer();
                    String line = null;
                    while ((line = in.readLine()) != null) {
                        response.append(line + MainConstants.kConstantNewLine);
                    }
                    responseString = response.toString();
                    if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                        Log.d("DSK ", "response:" + responseString);
                    }
                }
            }
        }
        return responseString;
    }
    //
//
    class RunInBackground extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            try
            {
                //
                String getUrl = MainConstants.sms_url+ MainConstants.sms_api_number+number
                        +MainConstants.sms_api_message+message;
                URL url = new URL(getUrl);
                String response = restAPICall(MainConstants.kConstantPostMethod,url,"");
                //
                Log.e("Log"," ***Get URL*** : "+getUrl);
                Log.e("Log"," ***Get Method response*** : "+response);

                //
            }
            catch (Exception e){
                Log.e("Log"," ***Get Method Error*** : "+e.getMessage());
            }
            return null;
        }
    }
    //
}
