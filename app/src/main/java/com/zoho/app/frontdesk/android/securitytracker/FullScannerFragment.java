package com.zoho.app.frontdesk.android.securitytracker;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.camera2.CameraManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class FullScannerFragment extends Fragment implements ZBarScannerView.ResultHandler {
    //
    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private static final String CAMERA_ID = "CAMERA_ID";
    private ZBarScannerView mScannerView;
    private boolean mFlash;
    private boolean mAutoFocus;
    private ArrayList<Integer> mSelectedIndices;
    private int mCameraId = -1;
    //
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {
        super.onCreateView(inflater, container, state);
        View view = inflater.inflate(R.layout.layout_barcode_fargment,container,false);
        mScannerView = new ZBarScannerView(getActivity());
        //
        if(state != null)
        {
            mFlash = state.getBoolean(FLASH_STATE, false);
            mAutoFocus = state.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = state.getIntegerArrayList(SELECTED_FORMATS);
            mCameraId = state.getInt(CAMERA_ID, -1);
        }
        else
        {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
            mCameraId = -1;
        }
        //

//        try
//        {
//            String cameraId = "";
//            CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
//            cameraId = manager.getCameraIdList()[1];
//            if(cameraId != null && !cameraId.isEmpty())
//            {
//                mCameraId = Integer.parseInt(cameraId);
//            }
//        }
//        catch (Exception e)
//        {
//            Log.e("TAG"," Camera id issue ******* "+e.getMessage());
//        }
        //

        //
        setupFormats();
        return mScannerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera(mCameraId);
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
        outState.putIntegerArrayList(SELECTED_FORMATS, mSelectedIndices);
        outState.putInt(CAMERA_ID, mCameraId);
    }

    @Override
    public void handleResult(Result rawResult) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getActivity().getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {

        }
        showAlertDialog(getActivity(),"Contents = " + rawResult.getContents() ,1);
        //showMessageDialog("Contents = " + rawResult.getContents() + ", Format = " + rawResult.getBarcodeFormat().getName());
    }

    public void showMessageDialog(String message) {
//        DialogFragment fragment = MessageDialogFragment.newInstance("Scan Results", message, this);
//        fragment.show(getActivity().getSupportFragmentManager(), "scan_results");
    }

    public void setupFormats() {
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        if(mSelectedIndices == null || mSelectedIndices.isEmpty()) {
            mSelectedIndices = new ArrayList<Integer>();
            for(int i = 0; i < BarcodeFormat.ALL_FORMATS.size(); i++) {
                mSelectedIndices.add(i);
            }
        }

        for(int index : mSelectedIndices) {
            formats.add(BarcodeFormat.ALL_FORMATS.get(index));
        }
        if(mScannerView != null) {
            mScannerView.setFormats(formats);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }
    //

    public void showAlertDialog(Context alert_context, String message, int code) {
        try{
            //here creating a layout with needed views and set it to alert dialog
            //if(dialog != null){ DialogView.hideDialog(); }
            ImageView alert_image = new ImageView(alert_context);
            alert_image.setImageResource(R.drawable.icon_flat_alert_yellow);
            alert_image.setPadding(20,20,20,20);
            TextView textView_msg = new TextView(alert_context);
            TextView textView_title = new TextView(alert_context);
            textView_title.setText("ALERT");
            textView_msg.setText(message);
            textView_msg.setTextSize(22);
            textView_title.setTextSize(26);
            textView_title.setPadding(20,20,20,20);
            textView_title.setGravity(Gravity.CENTER);
            textView_msg.setPadding(20,20,20,20);
            textView_msg.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                textView_msg.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
            }
            LinearLayout layout_alert = new LinearLayout(alert_context);
            layout_alert.setPadding(20, 20, 20, 20);
            layout_alert.setOrientation(LinearLayout.VERTICAL);
            layout_alert.addView(textView_title,0);
            layout_alert.addView(alert_image,1);
            layout_alert.addView(textView_msg,2);

            if(code > 0){
                AlertDialog alertDialog = new AlertDialog.Builder(alert_context)
                        .setView(layout_alert)
                        .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setNegativeButton("Try Again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(getActivity().getIntent());
                                getActivity().finish();
                            }
                        })
                        .create();

                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }
            else{
                AlertDialog alertDialog = new AlertDialog.Builder(alert_context)
                        .setView(layout_alert)
                        .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .create();

                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }
            //SingletonTTS.getInstance(getApplicationContext()).speakSentence(message);
        }
        catch (Exception e)
        {
            Log.e("Dialog"," *** error *** ------------ "+e.getMessage());
        }
    }
}