package com.zoho.app.frontdesk.android.securitytracker.backgroundProcess;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.zoho.app.frontdesk.android.securitytracker.Constants.MainConstants;
import com.zoho.app.frontdesk.android.securitytracker.Constants.NumberConstants;
import com.zoho.app.frontdesk.android.securitytracker.roomDB.DataBaseRepository;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class getEmployees {

    DataBaseRepository dataBaseRepository;
    Context context;
    //
    public getEmployees(Context context) {
        //
        this.context = context;
        dataBaseRepository = new DataBaseRepository(context);
        RunInBackground runInBackground = new RunInBackground();
        runInBackground.execute();
    }
    //

    /**
     * Call GET or POST method Rest API call
     *
     * @param restApiMethod String
     * @param url           URL
     * @param setParameter  String
     * @return responseString String
     * @throws IOException
     * @author ArumugaLingam R
     * @created 21/08/20
     */
    public String restAPICall(String restApiMethod, URL url, String setParameter) throws IOException {
        String responseString = MainConstants.kConstantEmpty;
//        if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
//            Log.d("DSK RestapiMethod", "" + restApiMethod);
//            Log.d("DSK RestapiURL", "" + url + setParameter);
//        }
        if (restApiMethod != null && !restApiMethod.isEmpty() && url != null && !url.toString().isEmpty()) {
            HttpURLConnection con = null;

            con = (HttpURLConnection) url.openConnection();
            if (con != null) {
                con.setDoOutput(true);
                con.setRequestMethod(restApiMethod);
                con.setConnectTimeout(NumberConstants.connectionTimeout);
                con.setReadTimeout(NumberConstants.connectionTimeout);

                if (restApiMethod == MainConstants.kConstantPostMethod && setParameter != null && !setParameter.isEmpty()) {
                    con.setRequestProperty(MainConstants.kConstantUserAgent,
                            MainConstants.user_Agent);
                    con.setRequestProperty(MainConstants.kConstantAcceptLanguage,
                            MainConstants.acceptLanguage);
                    DataOutputStream dataOutputStream = new DataOutputStream(
                            con.getOutputStream());
                    if (dataOutputStream != null) {
                        dataOutputStream.writeBytes(setParameter);
                        dataOutputStream.flush();
                        dataOutputStream.close();
                    }
                } else {
//                    con.connect();
                }
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.d("DSK restApiStatus ", "message " + con.getResponseMessage() + "");
                }
                int status = con.getResponseCode();
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.d("DSK restApiStatusCode", status + "");
                }
                if (status == NumberConstants.kConstantSuccessCode) {
                    // read the response
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));

                    StringBuffer response = new StringBuffer();
                    String line = null;
                    while ((line = in.readLine()) != null) {
                        response.append(line + MainConstants.kConstantNewLine);
                    }
                    responseString = response.toString();
                    if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                        Log.d("DSK ", "response:" + responseString);
                    }
                }
            }
        }
        return responseString;
    }
    //
    class RunInBackground extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            try
            {
              //
                String getUrl = MainConstants.get_url;
                URL url = new URL(getUrl);
                String response = restAPICall(MainConstants.kConstantGetMethod,url,"");
                response = response.substring(28,response.length()-1);
                //
                Log.e("Log"," ***Get URL*** : "+getUrl);
                Log.e("Log"," ***Get Method response*** : "+response);
                //
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("add_staff");
                //
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject_keys = jsonArray.getJSONObject(i);
                    String creator_id = jsonObject_keys.getString("ID");
                    String employee_id = jsonObject_keys.getString("staff_id");
                    String employee_name = jsonObject_keys.getString("staff_name");
                    String phone_number = jsonObject_keys.getString("contact_no");
                    String is_active = jsonObject_keys.getString("is_active");
                    String login_code = jsonObject_keys.getString("is_active");
                    Log.e("Log"," *** Employee *** : "+employee_name);
                    boolean isActive;
                    boolean isAdded;
                    if(is_active.equalsIgnoreCase("true")){
                        isActive = true;
                    }
                    else{
                        isActive = false;
                    }
                    //
                    if(dataBaseRepository.is_employee_available(employee_id)){
                        if(isActive) {
                            Log.e("Log"," *** Employee Updated *** : "+employee_name);
                            dataBaseRepository.updateEmployees(creator_id,employee_id,employee_name,phone_number,isActive,login_code);
                        }
                        else{
                            Log.e("Log"," *** Employee Deleted *** : "+employee_name);
                            dataBaseRepository.deleteEmployee(employee_id);
                        }
                    }
                    else
                        {
                        Log.e("Log"," *** Employee Added *** : "+employee_name);
                        dataBaseRepository.addEmployees(creator_id, employee_id, employee_name, phone_number, isActive,login_code);
                    }

                }
                //
            }
            catch (Exception e){
                Log.e("Log"," ***Get Method Error*** : "+e.getMessage());
            }
            return null;
        }
    }
}
