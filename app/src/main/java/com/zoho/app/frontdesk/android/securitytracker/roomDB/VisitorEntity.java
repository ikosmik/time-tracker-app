package com.zoho.app.frontdesk.android.securitytracker.roomDB;


import androidx.room.*;

// here setting table name
@Entity(tableName="visitor_db")
public class VisitorEntity {

    // here adding record id as auto generate id
    @PrimaryKey(autoGenerate = true)
    private int id;

    // here adding employee_id
    @ColumnInfo(name = "employee_id")
    private String employee_id;

    // here adding employee_name
    @ColumnInfo(name = "employee_name")
    private String employee_name;

    // here adding log_in_time
    @ColumnInfo(name = "log_in_time")
    private String log_in_time;

    // here adding log_out_time
    @ColumnInfo(name = "log_out_time")
    private String log_out_time;

    // here adding image_location
    @ColumnInfo(name = "image_location")
    private String image_location;

    // TODO remove - here adding attendance_count
    @ColumnInfo(name = "attendance_count")
    private int attendance_count;

    // here adding status
    @ColumnInfo(name = "status")
    private int status;

    // These are the getter ans setter functions for the table's columns

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getLog_in_time() {
        return log_in_time;
    }

    public void setLog_in_time(String log_in_time) {
        this.log_in_time = log_in_time;
    }

    public String getLog_out_time() {
        return log_out_time;
    }

    public void setLog_out_time(String log_out_time) {
        this.log_out_time = log_out_time;
    }

    public String getImage_location() {
        return image_location;
    }

    public void setImage_location(String image_location) {
        this.image_location = image_location;
    }

    public int getAttendance_count() {
        return attendance_count;
    }

    public void setAttendance_count(int attendance_count) {
        this.attendance_count = attendance_count;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
