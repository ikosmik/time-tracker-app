package com.zoho.app.frontdesk.android.securitytracker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.securitytracker.Constants.MainConstants;

public class MainActivity extends AppCompatActivity {

    private ImageView image_view_visitor;
    private RelativeLayout layout_pic,layout_home;
    private Button button_retake,button_send,button_show_employee,button_settings;
    private TextView text_title,text_home_title;
    private boolean is_pic_stored = false;
    private Handler brightnessDimHandler;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //here always keeping the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        }
        assign_object();
    }
    //
    private void assign_object()
    {
        //
        try{
            layout_home = findViewById(R.id.layout_home);
            layout_pic = findViewById(R.id.layout_pic);
            image_view_visitor = findViewById(R.id.image_view_visitor);
            button_show_employee = findViewById(R.id.button_show_employee);
            button_retake = findViewById(R.id.main_button_retake);
            button_send = findViewById(R.id.main_button_send);
            button_settings = findViewById(R.id.button_settings);
            text_title = findViewById(R.id.main_text_cam_title);
            layout_home.setOnTouchListener(onTouchListener);
            //
            button_show_employee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //call_cam();
                    Intent intent = new Intent(MainActivity.this,ActivityMenu.class);
                    startActivity(intent);
                    finish();
                }
            });
            //
            button_retake.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    call_cam();
                }
            });
            //
            button_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TODO send to zoho creator
                }
            });
            //
            button_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this,ActivitySettings.class);
                    startActivity(intent);
                    finish();
                }
            });
            //
            Log.e("LOG***"," count : "+is_pic_stored);
            Log.e("LOG***"," path : "+VisitorSingleton.getInstance().getVisitor_photo_path());
        }
        catch (Exception e){
            Log.e("LOG***"," Exception : "+e.getMessage());
        }

    }
    //
    private void call_cam(){
        VisitorSingleton.getInstance().clearInstance();
        Intent intent = new Intent(MainActivity.this,ActivityCamera2Api.class);
        startActivity(intent);
        finish();
    }
    //
    @Override
    protected void onResume() {
        super.onResume();
        if (!NetworkConnection.isWiFiEnabled(getApplicationContext())) {
            NetworkConnection.wifiReconnect(getApplicationContext());
        }
    }
    //
    @Override
    protected void onStop() {
        super.onStop();
        if (brightnessDimHandler != null && brightnessDimCallback != null) {
            brightnessDimHandler.removeCallbacks(brightnessDimCallback);
        }
    }
    //
    private void setBrightnessDimTimer() {
        Log.e("DIM"," *** setBrightnessDimTimer ***");
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = (float)255;
        getWindow().setAttributes(lp);
        brightnessDimHandler.removeCallbacks(brightnessDimCallback);
        brightnessDimHandler.postDelayed(brightnessDimCallback,
                MainConstants.BRIGHTNESS_DIM_TIMEOUT);
    }
    //
    private Runnable brightnessDimCallback = new Runnable() {

        public void run() {
            Log.e("Run method","*** brightnessDimCallback is called ***");
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = (float)0;
            getWindow().setAttributes(lp);
            // textViewManualEntryTitle.setTextColor(Color.parseColor("#FFFFFF"));
            layout_home.setOnTouchListener(onTouchListener);
        }
    };
    //
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            setBrightnessDimTimer();
            return false;
        }
    };
    //
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    //this function is used to check if network is available
    @NonNull
    public boolean checkConnectivity(){
        ConnectivityManager connMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network network = connMgr.getActiveNetwork();
            if (network == null){
                return false;
            }
            NetworkCapabilities capabilities = connMgr.getNetworkCapabilities(network);
            if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true;
            }
            else if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true;
            }
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager.getActiveNetworkInfo() != null){
                return connectivityManager.getActiveNetworkInfo().isConnected();
            }
            else{
                return false;
            }
        }
        return false;
    }
    //

    public void showAlertDialog(Context alert_context, String message) {
        try{
            //here creating a layout with needed views and set it to alert dialog
            //if(dialog != null){ DialogView.hideDialog(); }
            ImageView alert_image = new ImageView(alert_context);
            alert_image.setImageResource(R.drawable.icon_flat_alert_yellow);
            alert_image.setPadding(20,20,20,20);
            TextView textView_msg = new TextView(alert_context);
            TextView textView_title = new TextView(alert_context);
            textView_title.setText("ALERT");
            textView_msg.setText(message);
            textView_msg.setTextSize(22);
            textView_title.setTextSize(26);
            textView_title.setPadding(20,20,20,20);
            textView_title.setGravity(Gravity.CENTER);
            textView_msg.setPadding(20,20,20,20);
            textView_msg.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                textView_msg.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
            }
            LinearLayout layout_alert = new LinearLayout(alert_context);
            layout_alert.setPadding(20, 20, 20, 20);
            layout_alert.setOrientation(LinearLayout.VERTICAL);
            layout_alert.addView(textView_title,0);
            layout_alert.addView(alert_image,1);
            layout_alert.addView(textView_msg,2);

            AlertDialog alertDialog = new AlertDialog.Builder(alert_context)
                    .setView(layout_alert)
                    .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();

            //SingletonTTS.getInstance(getApplicationContext()).speakSentence(message);
        }
        catch (Exception e)
        {
            Log.e("Dialog"," *** error *** ------------ "+e.getMessage());
        }

    }
    //

    private void requestCameraPermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // Display a SnackBar with cda button to request the missing permission.
            Toast.makeText(this, "Camera access is required to Scan The Barcode.",
                    Toast.LENGTH_LONG).show();


            // Request the permission
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);

            startActivity(getIntent());
            finish();

        } else {
            Toast.makeText(this,
                    "<b>Camera could not be opened.</b>", Toast.LENGTH_SHORT).show();
            // Request the permission. The result will be received in onRequestPermissionResult().
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }
    }
}
