package com.zoho.app.frontdesk.android.securitytracker.Constants;

public class MainConstants {

    public static final String NetworkErrorMSG = "Oops!\nLooks like there\'s no internet connectivity, please connect with a network and try again.";
    public static final int BRIGHTNESS_DIM_TIMEOUT = 120000;
    public static final String kConstantEmpty = "";
    public static final String kConstantNewLine = "\n";
    public static final String kConstantGetMethod = "GET";
    public static final String kConstantPostMethod = "POST";
    public static final String user_Agent = "Mozilla/5.0";
    public static final String kConstantUserAgent = "User-Agent";
    public static final String kConstantAcceptLanguage = "Accept-Language";
    public static final String acceptLanguage = "en-US,en;q=0.5";

    public static final String app_authtoken = "8e98c8f9a9a8dbcd842ad9dca84ebe57";
    public static final String app_name = "staff-time-tracker";
    //
    public static final String sms_api_template = " is your login OTP.";
    public static final String sms_api_user = "zohocreator";
    public static final String sms_api_password = "World_123";
    public static final String sms_api_id = "3647853";
    public static final String sms_api_number = "&to=";
    public static final String sms_api_message = "&text=";
    public static final String sma_url2 = "https://platform.clickatell.com/messages/http/send?apiKey=xxxxxxxxxxxxxxxx==&to=xxxxxxxxxxx&content=Test+message+text";
    public static final String sms_url = "https://api.clickatell.com/http/sendmsg?user="+sms_api_user+"&password="+sms_api_password+"&api_id="+sms_api_id;
    public static final String post_url = "https://creator.zoho.com/api/cube_yogi/json/"+app_name+"/form/app_upload_data/record/add?authtoken="+app_authtoken+"&zc_ownername=cube_yogi&scope=creatorapi&Content-Type=application/json&json_data=";
    public static final String get_url = "https://creator.zoho.com/api/json/"+app_name+"/view/all_staffs?authtoken="+app_authtoken+"&zc_ownername=cube_yogi&scope=creatorapi&is_active=true";

}
