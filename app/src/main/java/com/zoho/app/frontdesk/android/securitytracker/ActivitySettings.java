package com.zoho.app.frontdesk.android.securitytracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.zoho.app.frontdesk.android.securitytracker.Constants.MainConstants;
import com.zoho.app.frontdesk.android.securitytracker.backgroundProcess.SendSMS;
import com.zoho.app.frontdesk.android.securitytracker.backgroundProcess.getEmployees;

public class ActivitySettings extends Activity {

    private Button button_sync;
    private ImageView back_button_settings;
    private getEmployees get_employees;
    private Context context;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //here always keeping the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.layout_settings);
        //
        context = this;
        assign_object();
    }
    //
    private void assign_object(){
        button_sync = findViewById(R.id.button_sync_staff);
        back_button_settings = findViewById(R.id.back_button_settings);
        //
        button_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                if(checkConnectivity()){
                    get_employees(context);
                }
                else{
                    showAlertDialog(context,MainConstants.NetworkErrorMSG);
                }

            }
        });
        //
        back_button_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivitySettings.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    //
    private void get_employees(Context context){
        get_employees = new getEmployees(context);
    }
    //
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    //this function is used to check if network is available
    @NonNull
    public boolean checkConnectivity(){
        ConnectivityManager connMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network network = connMgr.getActiveNetwork();
            if (network == null){
                return false;
            }
            NetworkCapabilities capabilities = connMgr.getNetworkCapabilities(network);
            if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true;
            }
            else if (capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true;
            }
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager.getActiveNetworkInfo() != null){
                return connectivityManager.getActiveNetworkInfo().isConnected();
            }
            else{
                return false;
            }
        }
        return false;
    }
    //

    public void showAlertDialog(Context alert_context, String message) {
        try{
            //here creating a layout with needed views and set it to alert dialog
            //if(dialog != null){ DialogView.hideDialog(); }
            ImageView alert_image = new ImageView(alert_context);
            alert_image.setImageResource(R.drawable.icon_flat_alert_yellow);
            alert_image.setPadding(20,20,20,20);
            TextView textView_msg = new TextView(alert_context);
            TextView textView_title = new TextView(alert_context);
            textView_title.setText("ALERT");
            textView_msg.setText(message);
            textView_msg.setTextSize(22);
            textView_title.setTextSize(26);
            textView_title.setPadding(20,20,20,20);
            textView_title.setGravity(Gravity.CENTER);
            textView_msg.setPadding(20,20,20,20);
            textView_msg.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                textView_msg.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
            }
            LinearLayout layout_alert = new LinearLayout(alert_context);
            layout_alert.setPadding(20, 20, 20, 20);
            layout_alert.setOrientation(LinearLayout.VERTICAL);
            layout_alert.addView(textView_title,0);
            layout_alert.addView(alert_image,1);
            layout_alert.addView(textView_msg,2);

            AlertDialog alertDialog = new AlertDialog.Builder(alert_context)
                    .setView(layout_alert)
                    .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();

            //SingletonTTS.getInstance(getApplicationContext()).speakSentence(message);
        }
        catch (Exception e)
        {
            Log.e("Dialog"," *** error *** ------------ "+e.getMessage());
        }

    }
}
