package com.zoho.app.frontdesk.android.securitytracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zoho.app.frontdesk.android.securitytracker.roomDB.DataBaseRepository;
import com.zoho.app.frontdesk.android.securitytracker.roomDB.EmployeeEntity;

import java.util.ArrayList;
import java.util.List;

public class ActivityMenu extends Activity {

    private RecyclerView recyclerViewMenu;
    private CustomRecyclerAdapter customRecyclerAdapter;
    private EmployeeEntity employeeEntity;
    private List<EmployeeEntity> employee_list;
    private DataBaseRepository dataBaseRepository;
    ArrayList<String> employee_names = new ArrayList<>();
    ArrayList<String> employee_id = new ArrayList<>();
    ArrayList<String> employee_phone_no = new ArrayList<>();
    ArrayList<String> employee_login_code = new ArrayList<>();
    private ImageView back_button_menu;
    Context context;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //here always keeping the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.layout_menu);
        setFinishOnTouchOutside(false);
        context = this;
        assign_objects();
    }
    //
    private void assign_objects(){
        back_button_menu = findViewById(R.id.back_button_menu);
        //
        back_button_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityMenu.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        //
        recyclerViewMenu = findViewById(R.id.recycler_meetings);
        dataBaseRepository = new DataBaseRepository(context);
        fetch_employees();
        show_employees();
        Log.e("success ", "Employees *********** " + employee_names.toString());
    }
    //
    private void fetch_employees()
    {
        try {
            employee_list = dataBaseRepository.fetchEmployees();
            //
            if (employee_list != null && employee_list.size() > 0) {
                //
                for (int i = 0; i < employee_list.size(); i++) {

                    //
                    if (employee_list.get(i).getEmployee_id() != null
                            && employee_list.get(i).getEmployee_name() != null && !employee_list.get(i).getEmployee_name().isEmpty()) {
                        employee_names.add(employee_list.get(i).getEmployee_name());
                        employee_id.add(employee_list.get(i).getEmployee_id());
                        employee_phone_no.add(employee_list.get(i).getPhone_number());
                        employee_login_code.add(employee_list.get(i).getLogin_code());
                    }
                }
            }
        }
        catch (Exception e){
            Log.e("Error ", "fetching Employees *********** " + e.getMessage());
        }
    }
    //
    private void show_employees(){
        try {
            //
            if (employee_list != null && employee_list.size() > 0) {
                //
                int span_count = 0;
                if(employee_list.size() >= 3){
                    span_count = 3;
                }
                else{
                    span_count = employee_list.size();
                }
                List<String> employee_details = employee_names;
                customRecyclerAdapter = new CustomRecyclerAdapter(context,employee_details);
                //
                customRecyclerAdapter.setClickListener(new CustomRecyclerAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        VisitorSingleton.getInstance().setEmployee_id(employee_id.get(position));
                        VisitorSingleton.getInstance().setEmployee_name(employee_names.get(position));
                        VisitorSingleton.getInstance().setEmployee_phone(employee_phone_no.get(position));
                        VisitorSingleton.getInstance().setEmployee_login_code(employee_login_code.get(position));
                        //
                        Intent intent = new Intent(ActivityMenu.this,ActivityBarCodeScan.class);
                        startActivity(intent);
                        finish();
                    }
                });
                // TODO remove below line
                span_count = 1;
                //
                recyclerViewMenu.setAdapter(customRecyclerAdapter);
                recyclerViewMenu.setLayoutManager(new GridLayoutManager(context, span_count));
            }
        }
        catch (Exception e){
            Log.e("Error ", "fetching Employees *********** " + e.getMessage());
        }
    }
    //
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
    //
}
