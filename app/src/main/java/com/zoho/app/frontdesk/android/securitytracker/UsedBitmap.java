package com.zoho.app.frontdesk.android.securitytracker;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.view.Surface;
import android.view.View;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

/*
 * Created by Karthick On 30/10/14
 */
public class UsedBitmap {
    public Integer newImageHeight = NumberConstants.kConstantZero;
    public Integer bitmapWidth;
    public Integer bitmapHeight;


    public Bitmap createBitmap(View v, boolean isWantBG) {

        //v.setVisibility(View.INVISIBLE);
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);
        if(isWantBG) {
            v.setBackgroundColor(Color.rgb(63,68,70));
        }
        Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), bitmap.getConfig());

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, NumberConstants.kConstantSignImageWidth, NumberConstants.kConstantSignImageHeight, false);
        bitmap.recycle();
        newBitmap.recycle();
        bitmap = null;
        newBitmap = null;
        return resizedBitmap;
    }



    /*
     *
     */
    public void storeBitmap(Bitmap bitmap, int newBitmapWidth, String fileName) {

        // Get bitmap width and height.
        Integer newImageHeight, bitmapWidth = bitmap.getWidth();
        Integer bitmapHeight = bitmap.getHeight();
        // if ((newBitmapWidth != 0) && (bitmapHeight != 0) && (bitmapWidth !=
        // 0))
        {
            newImageHeight = (newBitmapWidth * bitmapHeight) / bitmapWidth;

            if (newImageHeight != NumberConstants.kConstantZero) {

                Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                        newBitmapWidth, newImageHeight, true);
                bitmap.recycle();
                bitmap = null;
                bitmapWidth = resized.getWidth();
                bitmapHeight = resized.getHeight();
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                resized.compress(CompressFormat.JPEG,
                        NumberConstants.compressQuality, bytes);
                // String sourceFileUri="/sdcard/VisitorMgmt/"+ "ikosmik1.jpg";
                File file = new File(fileName);
                // Log.d("scanning activity", "uploadbusinesscard:" + fileName);
                // sourceFileUri=/sdcard/VisitorMgmt/+ "ikosmik1.jpg";

                try {
                    if (file.exists()) {
                        // Log.d("scanning activity", "exist:" + fileName);
                        file.delete();
                    }

                    file.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(
                            file);
                    fileOutputStream.write(bytes.toByteArray());
                    fileOutputStream.close();

                } catch (IOException e1) {

                    //

                }
                resized.recycle();
                resized = null;
                bytes = null;
                file = null;
            }
        }
    }

    /**
     *
     * @author Karthick
     * @created on 20161126
     *
     * @param path
     * @return byte[]
     */
    public byte[] getByteFromFile(String path) {

        File file = new File(path);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(
                    new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        return bytes;
    }

    /**
     *
     * @author Karthick
     * @created on 20161126
     *
     * @param path
     * @return byte[]
     */
    public boolean writeFile(byte[] fileBytes, String path) {
        BufferedOutputStream bos;
        try {
            File file = new File(path);
            if(file.exists()) {
                file.delete();
            }
            file.createNewFile();

            bos = new BufferedOutputStream(new FileOutputStream(file));

            bos.write(fileBytes);
            bos.flush();
            bos.close();
            //fileBytes=null;
            return true;
        } catch (FileNotFoundException e) {
//			Log.d("responses","responses"+e);
            return false;
        } catch (IOException e) {
//			Log.d("responses","responses:"+e);
            return false;
        }
    }

    /**
     * Resized image
     *
     * @author Karthick
     * @created on 20161126
     *
     * @param bitmap
     * @return byte[]
     */
    public byte[] getBytes(Bitmap bitmap) {
        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(CompressFormat.JPEG, NumberConstants.compressQuality, stream);
            bitmap.recycle();
            return stream.toByteArray();
        }
        return null;
    }

    /*
     *
     */
    public void storeBitmap(Bitmap bitmap, String fileName) {

        if(bitmap!=null) {

            int width=bitmap.getWidth(),height=bitmap.getHeight();
            if( bitmap.getWidth()>=4000) {
                width=bitmap.getWidth()/16;
                height=bitmap.getHeight()/16;
            } else if( bitmap.getWidth()<4000 && bitmap.getWidth()>=2000) {
                width=bitmap.getWidth()/10;
                height=bitmap.getHeight()/10;
            }
            else if( bitmap.getWidth()<2000 && bitmap.getWidth()>=1000) {
                width=bitmap.getWidth()/6;
                height=bitmap.getHeight()/6;
            } else if( bitmap.getWidth()<1000 && bitmap.getWidth()>=500) {
                width=bitmap.getWidth()/3;
                height=bitmap.getHeight()/3;
            }
            else if( bitmap.getWidth()<500 && bitmap.getWidth()>=300) {
                width=bitmap.getWidth()/2;
                height=bitmap.getHeight()/2;
            }
            Bitmap resizeBitmap = Bitmap.createScaledBitmap(
                    bitmap, width,height, false);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            resizeBitmap.compress(CompressFormat.JPEG,
                    NumberConstants.compressQuality, bytes);
            File file = new File(fileName);
            try {
                if (file.exists()) {
                    file.delete();
                }

                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(bytes.toByteArray());
                fileOutputStream.close();
                bytes.flush();
                bytes.close();
            } catch (IOException e) {

            }
            resizeBitmap.recycle();
            bitmap.recycle();
            bitmap = null;
            resizeBitmap=null;
            bytes = null;
            file = null;
        }

    }

    public byte[] storeResizedImage(byte[] fileUri, Integer rotation) {
        byte[] bytesArray = null;
        Matrix mat = new Matrix();

        if ((fileUri != null)
                && (fileUri.toString().length() > NumberConstants.kConstantZero)) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(fileUri,0,fileUri.length);
            int rotateangle = 0;
            //rotateangle = getDeviceRotationAngle(rotation);
            mat.setRotate(rotation, (float) bitmap.getWidth() / 2,
                    (float) bitmap.getHeight() / 2);

            if (bitmap != null) {
                Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                        bitmap.getWidth(), bitmap.getHeight(), mat, true);
                Bitmap resized = Bitmap.createScaledBitmap(newBitmap,
                        NumberConstants.visitorImageWidth,
                        NumberConstants.visitorImageHeight, true);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                resized.compress(CompressFormat.JPEG,
                        NumberConstants.compressQuality, bytes);
                if(bytes!=null) {
                    bytesArray=bytes.toByteArray();
                }
//					File imageFile = new File(fileUri);
//
//					imageFile.createNewFile();
//					FileOutputStream fileOutStream = new FileOutputStream(
//							imageFile);
//					fileOutStream.write(bytes.toByteArray());
//					fileOutStream.close();

                bitmap.recycle();
                bitmap = null;
                newBitmap.recycle();
                newBitmap = null;
                resized.recycle();
                resized = null;
            }
        }

        return bytesArray;
    }

    public int getDeviceRotationAngle(int rotation) {
        int rotateangle = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                // Log.d("orientation", " normal");
                break;
            case Surface.ROTATION_90:
                rotateangle = 90;

                // Log.d("orientation", " 90");

                break;
            case Surface.ROTATION_180:
                rotateangle = 180;
                // Log.d("orientation", " 180");
                break;
            case Surface.ROTATION_270:
                rotateangle = 270;
                // Log.d("orientation", " 270");
                break;
            default:
                // Log.d("orientation", " default");
                break;
        }
        return rotateangle;
    }

    /**
     * Returns bitmap as a roundShape
     *
     * @param bitmap
     *            pass bitmap as a original size, it used for resized bitmap as
     *            rounded shape
     * @return
     */
    public Bitmap getRoundedRectBitmap(Bitmap bitmap) {
        Bitmap result = null;
        try {
            result = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);

            int color = Color.GREEN;
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, 400, 400);

            paint.setAntiAlias(true);

            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(100, 100, 95, paint);
            // RectF oval = new RectF(80, 80, 80, 80);
            // canvas.drawOval(oval, paint);
            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            bitmap.recycle();
            bitmap = null;
        } catch (NullPointerException e) {
        } catch (OutOfMemoryError o) {
        }
        return result;
    }

    public Bitmap cropImage(Bitmap srcBmp) {
        Bitmap dstBmp = null;
        if (srcBmp != null) {
            // srcBmp
            // =BitmapFactory.decodeFile(StringConstants.kConstantFileDirectory
            // +StringConstants.kConstantFileFolder+file+
            // StringConstants.kConstantImageFileFormat);
            // Log.d("imageWidth",srcBmp.getWidth()+" height "+srcBmp.getHeight());
            if (srcBmp.getWidth() >= srcBmp.getHeight()) {

                // Log.d("if","crop");
                dstBmp = Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.getHeight(),
                        srcBmp.getHeight());

            } else {
                // Log.d("else","crop");
                dstBmp = Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.getWidth(),
                        srcBmp.getWidth());
            }
        }
        // storeBitmap(dstBmp, StringConstants.kConstantFileDirectory
        // +StringConstants.kConstantFileFolder+dstFileName+
        // StringConstants.kConstantImageFileFormat);
        return dstBmp;
    }

    // Load a bitmap from a resource with a target size
    public Bitmap decodeBitmap(String path, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    // Load a bitmap from a resource with a target size
    public Bitmap decodeBitmap(byte[] bitmapdata, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
        Bitmap resizeBitmap = Bitmap.createScaledBitmap(
                bitmap, reqWidth, reqHeight, false);
        bitmap.recycle();
        return resizeBitmap;
    }

    // Given the bitmap size and View size calculate a subsampling size (powers
    // of 2)
    int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
                              int reqHeight) {
        int inSampleSize = 1; // Default subsampling size
        // See if image raw height and width is bigger than that of required
        // view
        if (options.outHeight > reqHeight || options.outWidth > reqWidth) {
            // bigger
            final int halfHeight = options.outHeight / 2;
            final int halfWidth = options.outWidth / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    // convert from byte array to bitmap
    public Bitmap getImage(byte[] bytes) {
        if(bytes != null && bytes.length > NumberConstants.kConstantZero)
        {
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }
        else
        {
            return null;
        }
    }

    public Bitmap applyNoiceEffect(Bitmap source) {
        if(source!=null) {
            // get image size
            int width = source.getWidth();
            int height = source.getHeight();
            int[] pixels = new int[width * height];
            // get pixel array from source
            source.getPixels(pixels, 0, width, 0, 0, width, height);
            // a random object
            Random random = new Random();

            int index = 0;
            // iteration through pixels
            for(int y = 0; y < height; ++y) {
                for(int x = 0; x < width; ++x) {
                    // get current index in 2D-matrix
                    index = y * width + x;
                    // get random color
                    int colo =random.nextInt(255);
                    int randColor = Color.rgb(colo,
                            colo, colo);
                    // OR
                    pixels[index] |= randColor;
                }
            }
            // output bitmap
            Bitmap bmOut = Bitmap.createBitmap(width, height, source.getConfig());
            bmOut.setPixels(pixels, 0, width, 0, 0, width, height);
            source.recycle();
            pixels = new int[0];
            source=null;
            return bmOut;
        }
        return source;
    }

    public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                  int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSizes(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public int calculateInSampleSizes(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
