package com.zoho.app.frontdesk.android.securitytracker.roomDB;

import androidx.room.*;

import java.util.List;

@Dao
public interface EmployeeDatabaseAccessInterface
{
    // here using the table name
    final String table_name = "employee_db";

    // query to insert records
    @Insert
    void addRecord(EmployeeEntity entity);

    // query to update records
    @Update
    void updateRecord(EmployeeEntity entity);

    // query to delete records
    @Delete
    void deleteRecord(EmployeeEntity entity);

    // query to fetch records
    @Query("SELECT * FROM "+table_name+" where id = :id")
    List<EmployeeEntity> getRecordByID(String id);

    //
    @Query("SELECT * FROM "+table_name+" where employee_id = :id")
    List<EmployeeEntity> getRecordFromEmpID(String id);

    //
    @Query("SELECT * FROM "+table_name)
    List<EmployeeEntity> getAllEmployee();

    //
    @Query("SELECT COUNT(employee_id) FROM "+table_name+" where employee_id = :id")
    int isEmployeeFound(String id);

    //
    @Query("DELETE FROM "+table_name+" WHERE employee_id=:employee_id")
    void deleteEmployeeByID(String employee_id);

    // query to fetch records
    @Query("UPDATE "+table_name+" SET creator_id=:creator_id,employee_name=:employee_name,phone_number=:phone_number,is_active=:is_active,login_code=:login_code where employee_id = :employee_id")
    void updateEmployeeByID(String creator_id,String employee_id,String employee_name,String phone_number,String is_active,String login_code);

}