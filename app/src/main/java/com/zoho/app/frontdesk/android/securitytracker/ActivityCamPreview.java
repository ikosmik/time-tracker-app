package com.zoho.app.frontdesk.android.securitytracker;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.zoho.app.frontdesk.android.securitytracker.roomDB.DataBaseRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityCamPreview extends Activity {

    private Bitmap visitor_pic;
    private DataBaseRepository dataBaseRepository;
    private boolean pic_found = false;
    private ImageView image_view_preview;
    private TextView text_cam_title2;
    private Button button_cam_save,button_cam_retake;
    private final String json_time_pattern = "dd-MMM-yyyy HH:mm:ss";
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //here always keeping the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // here setting the XML layout
        setContentView(R.layout.layout_cam_preivew);
        context = this;
        assign_objects();
    }
    //
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
    //
    private void assign_objects(){
        image_view_preview = findViewById(R.id.image_view_preview);
        button_cam_retake = findViewById(R.id.button_cam_retake);
        button_cam_save = findViewById(R.id.button_cam_save);
        text_cam_title2 = findViewById(R.id.text_cam_title2);
        //
        getImageFromSingleton();
        //
        button_cam_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO insert data into db
                addImageToDB(context,visitor_pic);
                Intent intent = new Intent(ActivityCamPreview.this, MainActivity.class);
                startActivity(intent);
                finish();
                //saveImageToStorage(context,visitor_pic);
            }
        });

        button_cam_retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityCamPreview.this, ActivityCamera2Api.class);
                startActivity(intent);
                finish();
            }
        });
    }
    //
    private void getImageFromSingleton(){
        if(VisitorSingleton.getInstance().getVisitor_photo() != null
                && !VisitorSingleton.getInstance().getVisitor_photo().toString().isEmpty()){
            //byte[] person_photo_byte = VisitorSingleton.getInstance().getPerson_photo();
            visitor_pic = VisitorSingleton.getInstance().getVisitor_photo();
            image_view_preview.setImageBitmap(VisitorSingleton.getInstance().getVisitor_photo());
            button_cam_save.setEnabled(true);
            pic_found = true;
        }
        else{
            //image_view_preview.setImageResource(R.drawable.camera);
            button_cam_save.setEnabled(false);
            pic_found = false;
        }
    }
    //
    public static String saveImageToStorage(Context mContext, Bitmap bitmap){

        String mTimeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());

        String mImageName = "VISITOR_PIC"+mTimeStamp;

        ContextWrapper wrapper = new ContextWrapper(mContext);

        File file = wrapper.getDir("Time_Tracker_PICS",MODE_PRIVATE);

        file = new File(file, mImageName);

//        if(file.exists())
//        {
//            file.delete();
//        }

        try{

            OutputStream stream = null;

            stream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);

            stream.flush();

            stream.close();

        }catch (IOException e)
        {
            e.printStackTrace();
            Log.e("error","filepath ********* "+e.getMessage());
        }

        String mImageUri = file.getAbsolutePath();
        Log.e("filepath"," ********* "+mImageUri);
        return mImageUri;
    }
    //
    public String get_current_time()
    {
        //here creating a calendar object
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(json_time_pattern);
            String formattedDate = dateFormat.format(new Date());
            return formattedDate;
        }
        catch (Exception e){
            return "";
        }
    }
    //
    private void addImageToDB(Context context,Bitmap bmp)
    {
        String image_path = saveImageToStorage(context,bmp);
        dataBaseRepository = new DataBaseRepository(context);
        //
        dataBaseRepository.insert_record("test_id","test_name",
                get_current_time(),"",image_path,0,0);
    }
}
