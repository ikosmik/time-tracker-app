package com.zoho.app.frontdesk.android.securitytracker.roomDB;

import androidx.room.*;

import java.util.List;

@Dao
public interface DatabaseAccessInterface {

    // here using the table name
    final String table_name= "visitor_db";

    // query to insert records
    @Insert
    void addRecord(VisitorEntity entity);

    // query to update records
    @Update
    void updateRecord(VisitorEntity entity);

    // query to delete records
    @Delete
    void deleteRecord(VisitorEntity entity);

    // query to fetch records
    @Query("SELECT * FROM "+table_name+" where id = :id")
    List<VisitorEntity> getRecordByID(String id);

    //
    @Query("SELECT * FROM "+table_name+" where employee_id = :id")
    List<VisitorEntity> getRecordFromEmpID(String id);
}
