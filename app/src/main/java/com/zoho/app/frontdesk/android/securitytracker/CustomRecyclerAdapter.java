package com.zoho.app.frontdesk.android.securitytracker;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CustomRecyclerAdapter extends RecyclerView.Adapter<CustomRecyclerAdapter.ViewHolder> {

    private List<String> meetings;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // meetings is passed into the constructor
    CustomRecyclerAdapter(Context context, List<String> meetings) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.meetings = meetings;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.layout_recycler_adpater, parent, false);
        return new ViewHolder(view);
    }

    // binds the meetings to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String meeting_name = meetings.get(position);
        holder.text_meetings.setText(meeting_name);
        //holder.text_meetings.setTypeface(Typeface.createFromAsset(context.getAssets(), FontConstants.fontConstantProximaNovaBold));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return meetings.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView text_meetings;
        ViewHolder(View itemView) {
            super(itemView);
            text_meetings = itemView.findViewById(R.id.text_view_meeting_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting meetings at click position
    String getItem(int id) {
        return meetings.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}