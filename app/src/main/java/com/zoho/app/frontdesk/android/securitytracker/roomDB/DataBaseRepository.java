package com.zoho.app.frontdesk.android.securitytracker.roomDB;

import android.content.Context;

import androidx.room.Room;

import java.util.List;

public class DataBaseRepository {

    final String database_name = "visitor_db";
    final String emp_database_name = "employee_db";
    private AppDatabase appDatabase;
    private EmployeeAppDatabase employeeAppDatabase;
    private EmployeeEntity employeeEntity = new EmployeeEntity();
    private VisitorEntity entity = new VisitorEntity();
    // here creating a constructor for this class for calling from other activities
    public DataBaseRepository(Context context)
    {
        //here creating the database
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, database_name).allowMainThreadQueries().build();
        employeeAppDatabase = Room.databaseBuilder(context, EmployeeAppDatabase.class, emp_database_name).allowMainThreadQueries().build();
        }
    // This function is used to insert record into Room DB table
    public void insert_record(String employee_id,String employee_name,
                              String log_in_time,String log_out_time,
                              String image_path, int attendance_count,int status){
        entity.setEmployee_id(employee_id);
        entity.setEmployee_name(employee_name);
        entity.setLog_in_time(log_in_time);
        entity.setLog_out_time(log_out_time);
        entity.setImage_location(image_path);
        entity.setStatus(status);
        entity.setAttendance_count(attendance_count);
        // here adding the record
        appDatabase.appDatabaseObject().addRecord(entity);
    }
    // This function is used to update record into Room DB table
    public void update_record(){

    }
    // This function is used to delete records from Room DB table
    public void delete_records(){

    }
    //
    //
    public void addEmployees(String creator_id,String employee_id,String employee_name,String phone_number,boolean is_active,String login_code)
    {
        employeeEntity.setCreator_id(creator_id);
        employeeEntity.setEmployee_id(employee_id);
        employeeEntity.setEmployee_name(employee_name);
        employeeEntity.setPhone_number(phone_number);
        employeeEntity.setLogin_code(login_code);
        if(is_active){
            employeeEntity.setIs_active("true");
        }
        else{
            employeeEntity.setIs_active("false");
        }
        employeeAppDatabase.EmployeeAppDatabase().addRecord(employeeEntity);
    }
    //
    public List<EmployeeEntity> fetchEmployees(){

        return employeeAppDatabase.EmployeeAppDatabase().getAllEmployee();
    }
    //
    public void updateEmployees(String creator_id,String employee_id,String employee_name,String phone_number,boolean isActive,String login_code){

        String is_active = "";
        if(isActive){
            is_active = "true";
        }
        else{
            is_active = "false";
        }
        employeeAppDatabase.EmployeeAppDatabase().updateEmployeeByID(creator_id,employee_id,employee_name,phone_number,is_active,login_code);
    }
    //
    public void deleteEmployee(String employee_id){
        employeeAppDatabase.EmployeeAppDatabase().deleteEmployeeByID(employee_id);
    }
    //
    public boolean is_employee_available(String employee_id){
        //
        int found = employeeAppDatabase.EmployeeAppDatabase().isEmployeeFound(employee_id);
        if(found > 0){
            return true;
        }
        else{
            return false;
        }
    }
    //
}
