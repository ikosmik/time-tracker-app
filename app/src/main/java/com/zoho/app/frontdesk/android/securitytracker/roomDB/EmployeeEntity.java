package com.zoho.app.frontdesk.android.securitytracker.roomDB;


import androidx.room.*;

// here setting table name
@Entity(tableName="employee_db")
public class EmployeeEntity {

    // here adding record id as auto generate id
    @PrimaryKey(autoGenerate = true)
    private int id;

    // here adding employee_id
    @ColumnInfo(name = "creator_id")
    private String creator_id;

    // here adding employee_id
    @ColumnInfo(name = "employee_id")
    private String employee_id;

    // here adding employee_name
    @ColumnInfo(name = "employee_name")
    private String employee_name;

    // here adding employee_name
    @ColumnInfo(name = "phone_number")
    private String phone_number;

    // here adding employee_name
    @ColumnInfo(name = "login_code")
    private String login_code;

    // here adding employee_name
    @ColumnInfo(name = "is_active")
    private String is_active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getLogin_code() {
        return login_code;
    }

    public void setLogin_code(String login_code) {
        this.login_code = login_code;
    }
}
