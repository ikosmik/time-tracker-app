package com.zoho.app.frontdesk.android.securitytracker;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.ResultPoint;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.camera.CameraSettings;
import com.zoho.app.frontdesk.android.securitytracker.Constants.MainConstants;

import java.util.Arrays;
import java.util.List;

public class ActivityBarCodeScan extends Activity {

    private DecoratedBarcodeView barcodeReader;
    private ImageView back_button_scanner;
    private TextView text_view_scanner_title;
    private IntentIntegrator qrScan;
    private Button button_start_scan;
    Context context;
    private CaptureManager capture;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            // Remove the title bar.
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // hide the status bar.
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            //
            setContentView(R.layout.layout_scanner);
            this.context = this;
            assign_objects(savedInstanceState);
        }
        catch (Exception e){
            Log.e("ERROR", "******* Scanning setContentView Error : " + e.getMessage());
        }
    }
    //
    private void assign_objects(Bundle savedInstanceState){
        back_button_scanner = findViewById(R.id.back_button_scanner);
        text_view_scanner_title = findViewById(R.id.text_view_scanner_title);
        button_start_scan = findViewById(R.id.button_start_scan);
        //
        back_button_scanner.setImageResource(R.drawable.icon_flat_arrow_left);
        back_button_scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityBarCodeScan.this,ActivityMenu.class);
                startActivity(intent);
                finish();
            }
        });
        //
        button_start_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityBarCodeScan.this,SimpleScannerActivity.class);
                startActivity(intent);
                finish();
            }
        });
        //button_start_scan.setVisibility(View.GONE);
        //
        barcodeReader = (DecoratedBarcodeView)findViewById(R.id.barcode_scanner_view);
        qrScan = new IntentIntegrator(this);
        //StartScanning(savedInstanceState);
        openBarcodeFragment();
    }
    //
    private void startScan() {
        /**
         * Build a new MaterialBarcodeScanner
         */
        final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                .withActivity(ActivityBarCodeScan.this)
                .withEnableAutoFocus(true)
                .withBleepEnabled(true)
                .withFrontfacingCamera()
                .withText("Scanning...")
                .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                    @Override
                    public void onResult(Barcode barcode) {
                        //barcodeResult = barcode;
                        //result.setText(barcode.rawValue);
                        showAlertDialog(context,barcode.rawValue.toString(),1);
                    }
                })
                .build();
        materialBarcodeScanner.startScan();
    }
    //
    public void StartScanning(Bundle savedInstanceState){
        //barcodeReader.initializeFromIntent(qrScan.createScanIntent());
        try {
            qrScan.setCameraId(CameraSource.CAMERA_FACING_FRONT);
            CameraSettings cameraSettings = new CameraSettings();
            cameraSettings.setRequestedCameraId(1);
            qrScan.setPrompt("");
            qrScan.setBeepEnabled(true);
            qrScan.setBarcodeImageEnabled(true);
            qrScan.setOrientationLocked(false);
            barcodeReader.getBarcodeView().setCameraSettings(cameraSettings);
            barcodeReader.setFocusable(true);
            //qrScan.setCaptureActivity(getClass()).initiateScan();
            //qrScan.setTimeout(20000);
            capture = new CaptureManager(this, barcodeReader);
            capture.initializeFromIntent(qrScan.createScanIntent(), savedInstanceState);
            barcodeReader.decodeSingle(get_results);
            //qrScan.initiateScan();
            //start_timer();
        }
        catch (Exception e)
        {
            Log.e("ERROR","qrScan ********* "+e.getMessage());
        }
    }


    BarcodeCallback get_results = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            try {
                //here setting the time delay for action handler

                //Toast.makeText(this, "Scanned Info : "+barcode.displayValue.toString(), Toast.LENGTH_LONG).show();
                Log.e("Scanned", "******* result.getContents() : " +result.getText());
                if(result.getText() != null && !result.getText().isEmpty()){
                    //here storing scanned value to variable if it is not empty
                    String bar_code_value = result.getText();
                    String message = MainConstants.kConstantEmpty;
                    Log.e("Scanned", "******* bar_code_value : " + bar_code_value);
                    if(!bar_code_value.isEmpty()){
                        //here checking if scanned data is matches with our requirement using regex
                        showAlertDialog(context,"Barcode scanned successfully your login code : \n"+bar_code_value,0);
                    }
                    else{
                        //here setting error messages based on conditions
                        message = "Unable to Scan the BarCode, Tap \"Try again\" to scan";
                        showAlertDialog(context,message,1);
                        //here showing error messages on UI
                    }
                }
            }
            catch (Exception e){
                Log.e("ERROR", "******* Scanning Error : " + e.getMessage());
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }
    };
    //
    @Override
    public void onResume() {
        super.onResume();
        if(barcodeReader != null){
            barcodeReader.resume();
        }
        if(capture != null){

            capture.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(barcodeReader != null){
            barcodeReader.pause();
        }
        if(capture != null){
            capture.onPause();
        }
    }
    //
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(capture != null) {
            capture.onSaveInstanceState(outState);
        }
    }
    //
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeReader.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }
    //

    public void showAlertDialog(Context alert_context, String message,int code) {
        try{
            //here creating a layout with needed views and set it to alert dialog
            //if(dialog != null){ DialogView.hideDialog(); }
            ImageView alert_image = new ImageView(alert_context);
            alert_image.setImageResource(R.drawable.icon_flat_alert_yellow);
            alert_image.setPadding(20,20,20,20);
            TextView textView_msg = new TextView(alert_context);
            TextView textView_title = new TextView(alert_context);
            textView_title.setText("ALERT");
            textView_msg.setText(message);
            textView_msg.setTextSize(22);
            textView_title.setTextSize(26);
            textView_title.setPadding(20,20,20,20);
            textView_title.setGravity(Gravity.CENTER);
            textView_msg.setPadding(20,20,20,20);
            textView_msg.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                textView_msg.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
            }
            LinearLayout layout_alert = new LinearLayout(alert_context);
            layout_alert.setPadding(20, 20, 20, 20);
            layout_alert.setOrientation(LinearLayout.VERTICAL);
            layout_alert.addView(textView_title,0);
            layout_alert.addView(alert_image,1);
            layout_alert.addView(textView_msg,2);

            if(code > 0){
                AlertDialog alertDialog = new AlertDialog.Builder(alert_context)
                        .setView(layout_alert)
                        .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setNegativeButton("Try Again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(getIntent());
                                finish();
                            }
                        })
                        .create();

                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }
            else{
                AlertDialog alertDialog = new AlertDialog.Builder(alert_context)
                        .setView(layout_alert)
                        .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .create();

                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }

            //SingletonTTS.getInstance(getApplicationContext()).speakSentence(message);
        }
        catch (Exception e)
        {
            Log.e("Dialog"," *** error *** ------------ "+e.getMessage());
        }

    }
    //
    public void openBarcodeFragment() {

        try {
            // This function is used to open issual_selection_fragment
            FullScannerFragment fullScannerFragment = new FullScannerFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out
                    ,android.R.animator.fade_in,android.R.animator.fade_out);
            transaction.replace(R.id.fragment_barcode_scanner, fullScannerFragment);
            // transaction.addToBackStack(null);
            transaction.commit();
        } catch (Exception e) {
            //Log.e(" Error "," Fragment replacement *********** "+e.getMessage());
        }
    }
}
