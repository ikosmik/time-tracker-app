package com.zoho.app.frontdesk.android.securitytracker.roomDB;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = EmployeeEntity.class, version = 1, exportSchema = false)
public abstract class EmployeeAppDatabase extends RoomDatabase {
    public abstract EmployeeDatabaseAccessInterface EmployeeAppDatabase();
}
