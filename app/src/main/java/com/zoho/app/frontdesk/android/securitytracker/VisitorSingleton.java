package com.zoho.app.frontdesk.android.securitytracker;

import android.graphics.Bitmap;


public class VisitorSingleton {

    private static VisitorSingleton Visitor = null;

    private String kConstantEmpty = "";
    private Bitmap Visitor_photo;
    private String Visitor_photo_path;
    private byte[] person_photo;
    private int is_pic_saved;
    private String employee_id;
    private String employee_name;
    private String employee_phone;
    private String employee_login_code;
    private String otp;

    public static VisitorSingleton getInstance() {
        if (Visitor == null) {
            Visitor = new VisitorSingleton();
        }
        return Visitor;
    }

    public VisitorSingleton()
    {
        //clearInstance();
    }

    public void clearInstance()
    {
        //this function will clear all singleton variable's data
        Visitor_photo = null ;
        Visitor_photo_path=  kConstantEmpty;
        is_pic_saved= 0;
        employee_id = kConstantEmpty;
        employee_name = kConstantEmpty;
        employee_phone = kConstantEmpty;
        otp = kConstantEmpty;
    }

    public static VisitorSingleton getVisitor() {
        return Visitor;
    }

    public Bitmap getVisitor_photo() {
        return Visitor_photo;
    }

    public void setVisitor_photo(Bitmap visitor_photo) {
        Visitor_photo = visitor_photo;
    }

    public String getVisitor_photo_path() {
        return Visitor_photo_path;
    }

    public void setVisitor_photo_path(String visitor_photo_path) {
        Visitor_photo_path = visitor_photo_path;
    }

    public byte[] getPerson_photo() {
        return person_photo;
    }

    public void setPerson_photo(byte[] person_photo) {
        this.person_photo = person_photo;
    }
    //

    public int getIs_pic_saved() {
        return is_pic_saved;
    }
    //
    public void setIs_pic_saved(int is_pic_saved) {
        this.is_pic_saved = is_pic_saved;
    }

    public static void setVisitor(VisitorSingleton visitor) {
        Visitor = visitor;
    }

    public String getkConstantEmpty() {
        return kConstantEmpty;
    }

    public void setkConstantEmpty(String kConstantEmpty) {
        this.kConstantEmpty = kConstantEmpty;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_phone() {
        return employee_phone;
    }

    public void setEmployee_phone(String employee_phone) {
        this.employee_phone = employee_phone;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getEmployee_login_code() {
        return employee_login_code;
    }

    public void setEmployee_login_code(String employee_login_code) {
        this.employee_login_code = employee_login_code;
    }
}
