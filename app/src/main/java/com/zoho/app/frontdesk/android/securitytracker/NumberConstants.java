package com.zoho.app.frontdesk.android.securitytracker;

/**
 * Created by ArumugaLingam R on 28/01/20.
 */

public class NumberConstants {

    public static final int kConstantZero=0;
    public static final Integer kConstantProduction = 02;
    public static Integer connectionTimeout = 1000 * 30;
    public static final Integer kConstantSuccessCode = 200;
    public static final int compressQuality = 100;
    public static int visitorImageWidth = 450;
    public static int visitorImageHeight = 750;
    public static final int kConstantSignImageWidth = 300;
    public static final int kConstantSignImageHeight = 193;
}
