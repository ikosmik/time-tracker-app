package com.zoho.app.frontdesk.android.securitytracker.roomDB;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = VisitorEntity.class, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DatabaseAccessInterface appDatabaseObject();
}
